#include "servidor.h"
int seguir_jugando = SI;
int fueUtilizadaAntes(const char*, int, char);

int servidor() {
	salirHandler();
	sem_t* sem_servidor = sem_open(SERVIDOR, O_CREAT, SEM_PERMS, 1);

	sem_wait(sem_servidor);
	printf("Servidor iniciado\n");
	/*if (sem_trywait(sem_servidor) == 0) {
		printf("Iniciando servidor\n");
	}
	else {
		printf("Servidor ya abierto\n");
		sem_unlink(SERVIDOR);
		return EXIT_FAILURE;
	}*/

	signal(SIGINT, SIG_IGN);
	signal(SIGHUP, salirHandler);
	signal(SIGUSR1, finDeJuegoHandler);

	sem_t* sem_cliente = sem_open(CLIENTE, O_CREAT, SEM_PERMS, 0);
	sem_post(sem_cliente);

	do {
		jugar();
		sleep(10);
	} while (seguir_jugando == SI);

	sem_unlink(SERVIDOR);

	return EXIT_SUCCESS;
}

void jugar() {
	t_tablero tablero_servidor, tablero_cliente;
	sem_t* sem_consumidor_tablero = sem_open(CONSUMIDOR_TABLERO, O_CREAT, SEM_PERMS, 0);
	sem_t* sem_productor_tablero = sem_open(PRODUCTOR_TABLERO, O_CREAT, SEM_PERMS, 1);
	sem_t* sem_consumidor_casilla = sem_open(CONSUMIDOR_CASILLA, O_CREAT, SEM_PERMS, 0);
	sem_t* sem_productor_casilla = sem_open(PRODUCTOR_CASILLA, O_CREAT, SEM_PERMS, 1);
	int casilla_fd = shm_open(CASILLA, O_CREAT | O_RDONLY, 0600);
	int tablero_fd = shm_open(TABLERO, O_CREAT | O_RDWR, 0600);
	t_casilla casilla1, casilla2;

	ftruncate(tablero_fd, sizeof(t_tablero));

	tablero_servidor = generarNuevaPartida();
	tablero_cliente = inicializarTablero();
	while (faltaResolverTablero(&tablero_cliente)) {
		clrscr();
		mostrarTablero(&tablero_servidor);

		mostrarTablero(&tablero_cliente);

		sem_wait(sem_consumidor_casilla);
		casilla1 = leerCasillaDeMC(casilla_fd);
		sem_post(sem_productor_casilla);

		sem_wait(sem_productor_tablero);
		tablero_cliente = enviarTablero(tablero_cliente, &tablero_servidor, &casilla1, tablero_fd);
		sem_post(sem_consumidor_tablero);

		mostrarTablero(&tablero_cliente);

		sem_wait(sem_consumidor_casilla);
		casilla2 = leerCasillaDeMC(casilla_fd);
		sem_post(sem_productor_casilla);

		sem_wait(sem_productor_tablero);
		tablero_cliente = enviarTablero(tablero_cliente, &tablero_servidor, &casilla2, tablero_fd);
		sem_post(sem_consumidor_tablero);

		mostrarTablero(&tablero_cliente);

		if (compararCasillas(&tablero_servidor, &casilla1, &casilla2) != 0) {
			limpiarCasilla(&tablero_cliente, &casilla1);
			limpiarCasilla(&tablero_cliente, &casilla2);
		}

	}
	limpiarMemoria();
}

void finDeJuegoHandler() {
	seguir_jugando = NO;
}

t_tablero generarNuevaPartida() {
	int tam_tablero = TABLERO_SIZE * TABLERO_SIZE;
	int repeticiones = 2;
	int cant_letras = tam_tablero / repeticiones;
	char letra_aleatoria[cant_letras];
	t_tablero tablero;

	printf("Generando nueva partida\n");
	obtenerLetrasAleatorias(letra_aleatoria, cant_letras);
	for (int i = 0; i < cant_letras; ++i) printf("%d\n", letra_aleatoria[i]);
	tablero = crearTableroConLetras(letra_aleatoria, cant_letras, repeticiones);
	tablero = ordenarTableroAleatoriamente(tablero, TABLERO_SIZE);

	return tablero;
}

t_tablero enviarTablero(t_tablero tablero_cliente, const t_tablero* tablero_servidor, const t_casilla* casilla, int tablero_fd) {
	char caracter = obtenerCaracter(tablero_servidor, casilla);
	modificarCaracter(&tablero_cliente, casilla, caracter);
	escribirTableroEnMC(tablero_cliente, tablero_fd);
	return tablero_cliente;
}

t_tablero crearTableroConLetras(char* letras, int cant_letras, int repeticiones) {
	int i, j, pos, fila, columna;
	t_tablero tablero;

	printf("Creando tablero con letras\n");

	for (i = 0; i < cant_letras; ++i) {
		for (j = 0; j < repeticiones; ++j) {
			pos = i * 2 + j;
			fila = pos / 4;
			columna = pos % 4;
			tablero.data[fila][columna] = letras[i];
		}
	}

	return tablero;
}

t_tablero ordenarTableroAleatoriamente(t_tablero tablero, int cant_filas_columnas) {
	int i, j, random_index, fila, columna;
	int tam_tablero = cant_filas_columnas * cant_filas_columnas;
	char temp;

	printf("Ordenando tablero aleatoriamente\n");

	srand(time(NULL));

	for (i = 0; i < cant_filas_columnas; ++i) {
		for (j = 0; j < cant_filas_columnas; ++j) {
			temp = tablero.data[i][j];
			random_index = rand() % tam_tablero;

			fila = random_index / cant_filas_columnas;
			columna = random_index % cant_filas_columnas;

			tablero.data[i][j] = tablero.data[fila][columna];
			tablero.data[fila][columna] = temp;
		}
	}

	return tablero;
}

void obtenerLetrasAleatorias(char* letras, int cant_letras) {
	int i, j;
	char letra;

	srand(time(NULL));

	printf("Obteniendo letras aleatorias\n");

	for (i = 0; i < cant_letras; ++i) {
		do {
			letra = 'A' + (rand() % 26);
		} while(fueUtilizadaAntes(letras, i, letra) == SI);

		letras[i] = letra;
	}
}

int fueUtilizadaAntes(const char* letras, int pos, char letra) {
	for (int i = 0; i < pos; ++i) {
		if (letras[i] == letra) return SI;
	}

	return NO;
}

char* usage(char* app_name) {
	char* usage = app_name;
	char* tmp = strdup(app_name);
	strcpy(usage, "Uso: ");
	strcat(usage, tmp);
	free(tmp);

	return usage;
}

void help(char* app_name) {
	printf("\n");

	printf("%s\n", usage(app_name));

	printf("\n");

	printf("- Encargado de actualizar el estado del tablero en base al par de casillas ingresado\n");
	printf("- Controla la finalización de la partida de Memotest\n");

	printf("\n");

	printf("Crea un tablero de 16 casillas (4 filas x 4 columnas)\n");
	printf("Genera una partida rellenando aleatoriamente un tablero con 8 pares de letras mayúsculas (A-Z)\n");
	printf("Cada letra aparece solo dos veces en posiciones aleatorias\n");

	printf("\n");

	printf("- Ejecuta un solo cliente\n");
	printf("- Puede haber uno solo abierto\n");
	printf("- Queda a la espera de que un cliente se ejecute\n");
	printf("- Ignora la señal SIGINT (CTRL-C)\n");
	printf("- Finalizará al recibir una señal SIGUSR1, siempre y cuando no haya ninguna partida en progreso\n");

	printf("\n");
}

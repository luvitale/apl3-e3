#ifndef CLIENTE_H_INCLUDED
#define CLIENTE_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include "utils.h"

int cliente();
void jugar();
int seguirJugando();
t_casilla enviarCasilla(int);
t_casilla pedirCasilla();
pid_t obtenerPID(char*);

char* usage(char*);
void help();

#endif // CLIENTE_H_INCLUDED

#include "cliente.h"

int main(int argc, char* argv[]) {
	char app_name[32];
	strcpy(app_name, argv[0]);

	if (argc == 1) {
		printf("Iniciando cliente\n");
		return cliente();
	}
	else if ( argc == 2 && ( strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0 ) ) {
		help(app_name);
		return EXIT_SUCCESS;
	}
	else {
		printf("Parámetros ingresados incorrectamente\n\n");
		printf("%s\n",usage(app_name));
		return EXIT_FAILURE;
	}
}

#include "utils.h"

int faltaResolverTablero(const t_tablero* tablero) {
	for (int i = 0; i < TABLERO_SIZE; ++i) {
		for (int j = 0; j < TABLERO_SIZE; ++j) {
			if (tablero->data[i][j] == '\0') return SI;
		}
	}

	return NO;
}

t_tablero inicializarTablero() {
	printf("Inicializando tablero\n");
	t_casilla casilla;
	t_tablero tablero;

	for (int i = 0; i < TABLERO_SIZE; ++i) {
		casilla.fila = i;
		for (int j = 0; j < TABLERO_SIZE; ++j) {
			casilla.columna = j;
			limpiarCasilla(&tablero, &casilla);
		}
	}

	return tablero;
}

int compararCasillas(const t_tablero* tablero, const t_casilla* casilla1, const t_casilla* casilla2) {
	return obtenerCaracter(tablero, casilla1) - obtenerCaracter(tablero, casilla2);
}

void modificarCaracter(t_tablero* tablero, const t_casilla* casilla, char caracter) {
	tablero->data[casilla->fila][casilla->columna] = caracter;
}

char obtenerCaracter(const t_tablero* tablero, const t_casilla* casilla) {
	return tablero->data[casilla->fila][casilla->columna];
}

void limpiarCasilla(t_tablero* tablero, const t_casilla* casilla) {
	tablero->data[casilla->fila][casilla->columna] = '\0';
}

void mostrarTablero(const t_tablero* tablero) {
	int i, j;
	char caracter;

	printf(" ");
	for (j = 0; j < TABLERO_SIZE; ++j) {
		printf("\t%d",j);
	}
	printf("\n");

	for (i = 0; i < TABLERO_SIZE; ++i) {
		printf("%d",i);
		for (j = 0; j < TABLERO_SIZE; ++j) {
			caracter = tablero->data[i][j];
			if (caracter) {
				printf("\t%c",caracter);
			}
			else {
				printf("\t?");
			}
		}
		printf("\n");
	}
}

void limpiarMemoria() {
	sem_unlink(PRODUCTOR_TABLERO);
	sem_unlink(PRODUCTOR_CASILLA);
	sem_unlink(CONSUMIDOR_TABLERO);
	sem_unlink(CONSUMIDOR_CASILLA);
	shm_unlink(TABLERO);
	shm_unlink(CASILLA);
}

void salirHandler() {
	limpiarMemoria();
	sem_unlink(SERVIDOR);
	sem_unlink(CLIENTE);
}

// Memoria compartida
t_casilla leerCasillaDeMC(int casilla_fd) {
	printf("Leyendo casilla de memoria compartida\n");
	t_casilla* ptr = mmap(0, sizeof(t_casilla), PROT_READ, MAP_SHARED, casilla_fd, 0);
	t_casilla casilla = *ptr;
	munmap(ptr, sizeof(t_casilla));
	return casilla;
}

void escribirCasillaEnMC(const t_casilla casilla, int casilla_fd) {
	printf("Escribiendo casilla en memoria compartida\n");
	t_casilla* ptr;
	ptr = mmap(0, sizeof(casilla), PROT_WRITE, MAP_SHARED, casilla_fd, 0);
	memcpy(ptr, &casilla, sizeof(casilla));
	munmap(ptr, sizeof(casilla));
}

t_tablero leerTableroDeMC(int tablero_fd) {
        printf("Leyendo tablero de memoria compartida\n");
        t_tablero* ptr = mmap(0, sizeof(t_tablero), PROT_READ, MAP_SHARED, tablero_fd, 0);
	t_tablero tablero = *ptr;
	munmap(ptr, sizeof(t_tablero));
	return tablero;
}

void escribirTableroEnMC(const t_tablero tablero, int tablero_fd) {
	printf("Escribiendo tablero en memoria compartida\n");

	t_tablero* ptr;
	ptr = mmap(0, sizeof(tablero), PROT_WRITE, MAP_SHARED, tablero_fd, 0);
	memcpy(ptr, &tablero, sizeof(tablero));
	munmap(ptr, sizeof(tablero));
}

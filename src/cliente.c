#include "cliente.h"
void presionarParaContinuar();

int cliente() {
	sem_t* sem_cliente = sem_open(CLIENTE, O_CREAT, SEM_PERMS, 0);

	sem_wait(sem_cliente);
	printf("Memotest\n");
	/*if (sem_trywait(sem_cliente) == 0) {
		printf("Memotest\n");
	}
	else {
		printf("No se pudo abrir el cliente\n");
		printf("Es necesario que se haya iniciado el servidor y que no esté en ejecución otro cliente\n");
		sem_unlink(CLIENTE);
		return EXIT_FAILURE;
	}*/

	signal(SIGINT, SIG_IGN);
	signal(SIGHUP, salirHandler);

	do {
		jugar();
	} while(seguirJugando());

	sem_unlink(CLIENTE);

	return EXIT_SUCCESS;
}

void jugar() {
	t_tablero tablero;
	sem_t* sem_consumidor_tablero = sem_open(CONSUMIDOR_TABLERO, O_CREAT, SEM_PERMS, 0);
	sem_t* sem_productor_tablero = sem_open(PRODUCTOR_TABLERO, O_CREAT, SEM_PERMS, 1);
	sem_t* sem_consumidor_casilla = sem_open(CONSUMIDOR_CASILLA, O_CREAT, SEM_PERMS, 0);
	sem_t* sem_productor_casilla = sem_open(PRODUCTOR_CASILLA, O_CREAT, SEM_PERMS, 1);
	int casilla_fd = shm_open(CASILLA, O_CREAT | O_RDWR, 0600);
	int tablero_fd = shm_open(TABLERO, O_CREAT | O_RDONLY, 0600);
	t_casilla casilla1, casilla2;

	ftruncate(casilla_fd, sizeof(t_casilla));

	tablero = inicializarTablero();
	do {
		clrscr();
		mostrarTablero(&tablero);

		sem_wait(sem_productor_casilla);
		casilla1 = enviarCasilla(casilla_fd);
		sem_post(sem_consumidor_casilla);

		sem_wait(sem_consumidor_tablero);
		tablero = leerTableroDeMC(tablero_fd);
		sem_post(sem_productor_tablero);

		clrscr();
		mostrarTablero(&tablero);

		sem_wait(sem_productor_casilla);
		casilla2 = enviarCasilla(casilla_fd);
		sem_post(sem_consumidor_casilla);

		sem_wait(sem_consumidor_tablero);
		tablero = leerTableroDeMC(tablero_fd);
		sem_post(sem_productor_tablero);

		clrscr();
		mostrarTablero(&tablero);

		if (compararCasillas(&tablero, &casilla1, &casilla2) != 0) {
			limpiarCasilla(&tablero, &casilla1);
			limpiarCasilla(&tablero, &casilla2);
		}

		presionarParaContinuar();
	} while(faltaResolverTablero(&tablero));
}

void presionarParaContinuar() {
	printf("Presioná ENTER para continuar...");
	fflush(stdin);
	getchar();
	getchar();
}

int seguirJugando() {
	char caracter;

	printf("¿Querés seguir jugando? (S/n): ");
	scanf("%c",&caracter);
	if (caracter == 'n' || caracter == 'N') {
		pid_t pid_servidor = obtenerPID(NOMBRE_SERVIDOR);
		kill(pid_servidor, SIGUSR1);
		return NO;
	}
	else {
		return SI;
	}
}

t_casilla enviarCasilla(int casilla_fd) {
	t_casilla casilla = pedirCasilla();
	escribirCasillaEnMC(casilla, casilla_fd);
	return casilla;
}

t_casilla pedirCasilla() {
	t_casilla casilla;

	do {
		printf("Ingresar fila: ");
		scanf("%d",&casilla.fila);
	} while(casilla.fila < 0 || casilla.fila >= TABLERO_SIZE);

	do {
		printf("Ingresar columna: ");
		scanf("%d",&casilla.columna);
	} while(casilla.columna < 0 || casilla.columna >= TABLERO_SIZE);

	return casilla;
}

pid_t obtenerPID(char* nombre) {
	char buf[512];
	char cmd[100] = "pidof -s ";
	strcat(cmd, nombre);
	FILE *cmd_pipe = popen(cmd, "r");

	fgets(buf, 512, cmd_pipe);
	pid_t pid = strtoul(buf, NULL, 10);

	pclose(cmd_pipe);

	return pid;
}

char* usage(char* app_name) {
	char* usage = app_name;
	char* tmp = strdup(app_name);
	strcpy(usage, "Uso: ");
	strcat(usage, tmp);
	free(tmp);

	return usage;
}

void help(char* app_name) {
	printf("\n");

	printf("%s\n", usage(app_name));

	printf("\n");

	printf("- Muestra por pantalla el estado actual del tablero\n");
	printf("- Lee desde el teclado el par de casillas que el usuario quiere destapar\n");

	printf("\n");

	printf("Tiene que encontrar los pares de casillas iguales en un tablero de 16 casillas (4 filas x 4 columnas)\n");

	printf("\n");

	printf("- Ignora la señal SIGINT (CTRL-C)\n");
	printf("- Muestra el tiempo para saber cuánto se tarda en resolver el juego\n");

	printf("\n");
}

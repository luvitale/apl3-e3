#ifndef SERVIDOR_H_INCLUDED
#define SERVIDOR_H_INCLUDED

#include "utils.h"

int servidor();

char* usage(char*);
void help(char*);

void jugar();
void finDeJuegoHandler();

t_tablero enviarTablero(t_tablero, const t_tablero*, const t_casilla*, int);
t_tablero generarNuevaPartida();
t_tablero crearTableroConLetras(char*, int, int);
t_tablero ordenarTableroAleatoriamente(t_tablero, int);
void obtenerLetrasAleatorias(char*, int);

#endif // SERVIDOR_H_INCLUDED

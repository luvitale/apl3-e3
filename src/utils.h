#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */
#include <semaphore.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#define SEM_PERMS (S_IRUSR | S_IWUSR)
#define PRODUCTOR_TABLERO "/productor_tablero"
#define PRODUCTOR_CASILLA "/productor_casilla"
#define CONSUMIDOR_TABLERO "/consumidor_tablero"
#define CONSUMIDOR_CASILLA "/consumidor_casilla"
#define SERVIDOR "/servidor"
#define CLIENTE "/cliente"
#define CASILLA "/casilla"
#define TABLERO "/tablero"
#define BUFFER_SIZE 4096
#define TABLERO_SIZE 4
#define SI 1
#define NO 0
#define NOMBRE_SERVIDOR "./servidor.app"
#ifdef _WIN32
#include <conio.h>
#else
#include <stdio.h>
#define clrscr() printf("\e[1;1H\e[2J")
#endif

typedef struct {
	char data[TABLERO_SIZE][TABLERO_SIZE];
} t_tablero;
typedef struct {
	int fila;
	int columna;
} t_casilla;

int faltaResolverTablero(const t_tablero*);
t_tablero inicializarTablero();
int compararCasillas(const t_tablero*, const t_casilla*, const t_casilla*);
void modificarCaracter(t_tablero*, const t_casilla*, char);
char obtenerCaracter(const t_tablero*, const t_casilla*);
void limpiarCasilla(t_tablero*, const t_casilla*);
void mostrarTablero(const t_tablero*);
void limpiarMemoria();
void salirHandler();

// Memoria compartida
t_casilla leerCasillaDeMC(int);
void escribirCasillaEnMC(const t_casilla, int);

t_tablero leerTableroDeMC(int);
void escribirTableroEnMC(const t_tablero, int);

#endif // UTILS_H_INCLUDED

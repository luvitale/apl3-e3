#include <stdlib.h>
#include <stdint.h>
#include <check.h>
#include "../src/cliente.h"

void setup(void) {
	return;
}

void teardown(void) {
	return;
}

START_TEST(test_usage) {
	char app_name[32] = "./cliente.app";

	ck_assert(strcmp(usage(app_name), "Uso: ./cliente.app") == 0);
}
END_TEST

START_TEST(test_write_square_in_shm) {
	t_casilla casilla_enviada;
	casilla_enviada.fila = 1;
	casilla_enviada.columna = 2;

	int casilla_rdwr = shm_open(CASILLA, O_CREAT | O_RDWR, 0600);
	ftruncate(casilla_rdwr, sizeof(casilla_enviada));
	escribirCasillaEnMC(casilla_enviada, casilla_rdwr);

	int casilla_rdonly = shm_open(CASILLA, O_CREAT | O_RDONLY, 0600);
	t_casilla casilla_recibida = leerCasillaDeMC(casilla_rdonly);

	shm_unlink(CASILLA);

	ck_assert_int_eq(casilla_enviada.fila, casilla_recibida.fila);
	ck_assert_int_eq(casilla_enviada.columna, casilla_recibida.columna);
}
END_TEST

Suite * make_cliente_creation_suite(void) {
  Suite *s;
  TCase *tc_core;

  s = suite_create("Cliente Test Suite");

  /* Creation test case */
  tc_core = tcase_create("Cliente Test Cases");

  tcase_add_checked_fixture(tc_core, setup, teardown);
  tcase_add_test(tc_core, test_usage);
  tcase_add_test(tc_core, test_write_square_in_shm);

  suite_add_tcase(s, tc_core);

  return s;
}

int main(void) {
  int number_failed;
  SRunner *sr;

  sr = srunner_create(make_cliente_creation_suite());
  srunner_set_fork_status(sr, CK_NOFORK);
  srunner_set_log (sr, "cliente_test.log");
  srunner_set_xml (sr, "cliente_test.xml");
  srunner_run_all(sr, CK_VERBOSE);

  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

#include <stdlib.h>
#include <stdint.h>
#include <check.h>
#include "../src/servidor.h"

void setup(void) {
	return;
}

void teardown(void) {
	return;
}

START_TEST(test_usage) {
	char app_name[32] = "./servidor.app";

	ck_assert_str_eq(usage(app_name), "Uso: ./servidor.app");
}
END_TEST

START_TEST(test_random_characters) {
	int i;
	int character_size = TABLERO_SIZE * TABLERO_SIZE / 2;
	char caracteres[character_size];

	obtenerLetrasAleatorias(caracteres, character_size);

	for (i = 0; i < character_size; ++i) {
		ck_assert_uint_ge(caracteres[i], 'A');
		ck_assert_uint_le(caracteres[i], 'Z');
	}
}
END_TEST

START_TEST(test_create_board_with_characters) {
	int i, j, pos, fila, columna;
	int cant_letras = 8;
	char letras[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};
	int repeticiones = 2;
	t_tablero tablero;

	tablero = crearTableroConLetras(letras, cant_letras, repeticiones);

	for (i = 0; i < cant_letras; ++i) {
		for (j = 0; j < repeticiones; ++j) {
			pos = i * 2 + j;
			fila = pos / 4;
			columna = pos % 4;

			ck_assert_uint_eq(tablero.data[fila][columna], letras[i]);
		}
	}
}
END_TEST

START_TEST(test_get_board_with_square) {
	int cant_letras = 8;
	char letras[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};
	int repeticiones = 2;
	t_tablero tablero_servidor;

	tablero_servidor = crearTableroConLetras(letras, cant_letras, repeticiones);

	t_casilla casilla;
	casilla.fila = 1;
	casilla.columna = 1;
	char letra_casilla = 'C';

	t_tablero tablero_cliente;
	tablero_cliente = inicializarTablero();

	modificarCaracter(&tablero_cliente, &casilla, obtenerCaracter(&tablero_servidor, &casilla));

	for (int i = 0; i < TABLERO_SIZE; ++i) {
		for (int j = 0; j < TABLERO_SIZE; ++j) {
			if (casilla.fila == i && casilla.columna == j) {
				ck_assert_uint_eq(tablero_cliente.data[i][j], letra_casilla);
			}
			else {
				ck_assert_uint_eq(tablero_cliente.data[i][j], '\0');
			}
		}
	}
}
END_TEST

START_TEST(test_compare_squares) {
	int cant_letras = 8;
	char letras[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};
	int repeticiones = 2;
	t_tablero tablero;

	tablero = crearTableroConLetras(letras, cant_letras, repeticiones);

	t_casilla casilla1;
	casilla1.fila = 1;
	casilla1.columna = 0;

	t_casilla casilla2;
	casilla2.fila = 1;
	casilla2.columna = 1;

	ck_assert_int_eq(compararCasillas(&tablero, &casilla1, &casilla2), 0);

	casilla2.fila = 2;
	casilla2.columna = 2;

	ck_assert_int_ne(compararCasillas(&tablero, &casilla1, &casilla2), 0);
}
END_TEST

START_TEST(test_write_board_in_shm) {
	int cant_letras = 8;
	char letras[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};
	int repeticiones = 2;
	t_tablero tablero_servidor;

	tablero_servidor = crearTableroConLetras(letras, cant_letras, repeticiones);

	t_casilla casilla;
	casilla.fila = 1;
	casilla.columna = 1;
	char letra_casilla = 'C';

	t_tablero tablero_cliente;
	tablero_cliente = inicializarTablero();

	int tablero_rdwr = shm_open(TABLERO, O_CREAT | O_RDWR, 0600);
	ftruncate(tablero_rdwr, sizeof(tablero_cliente));
	tablero_cliente = enviarTablero(tablero_cliente, &tablero_servidor, &casilla, tablero_rdwr);

	int tablero_rdonly = shm_open(TABLERO, O_CREAT | O_RDONLY, 0600);
	t_tablero tablero_resultante;
	tablero_resultante = leerTableroDeMC(tablero_rdonly);

	shm_unlink(TABLERO);

	for (int i = 0; i < TABLERO_SIZE; ++i) {
		for (int j = 0; j < TABLERO_SIZE; ++j) {
			if (casilla.fila == i && casilla.columna == j) {
				ck_assert_uint_eq(tablero_resultante.data[i][j], letra_casilla);
			}
			else {
				ck_assert_uint_eq(tablero_resultante.data[i][j], '\0');
			}
		}
	}
}
END_TEST

START_TEST(test_new_match) {
	t_tablero tablero;
	int i, j;
	tablero = generarNuevaPartida();

	for (i = 0; i < TABLERO_SIZE; ++i) {
		for (j = 0; j < TABLERO_SIZE; ++j) {
			ck_assert_uint_ge(tablero.data[i][j], 'A');
			ck_assert_uint_le(tablero.data[i][j], 'Z');
		}
	}
}
END_TEST

Suite * make_servidor_creation_suite(void) {
  Suite *s;
  TCase *tc_core;

  s = suite_create("Servidor Test Suite");

  /* Creation test case */
  tc_core = tcase_create("Servidor Test Cases");

  tcase_add_checked_fixture(tc_core, setup, teardown);
  tcase_add_test(tc_core, test_usage);
  tcase_add_test(tc_core, test_random_characters);
  tcase_add_test(tc_core, test_create_board_with_characters);
  tcase_add_test(tc_core, test_get_board_with_square);
  tcase_add_test(tc_core, test_compare_squares);
  tcase_add_test(tc_core, test_write_board_in_shm);
  tcase_add_test(tc_core, test_new_match);

  suite_add_tcase(s, tc_core);

  return s;
}

int main(void) {
  int number_failed;
  SRunner *sr;

  sr = srunner_create(make_servidor_creation_suite());
  srunner_set_fork_status(sr, CK_NOFORK);
  srunner_set_log (sr, "servidor_test.log");
  srunner_set_xml (sr, "servidor_test.xml");
  srunner_run_all(sr, CK_VERBOSE);

  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

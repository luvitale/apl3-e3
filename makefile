CC = gcc
CFLAGS = -g
SDIR = ./src
ODIR = ./obj
TESTDIR = ./test
OSERVIDOR = ${ODIR}/main_servidor.o ${ODIR}/servidor.o ${ODIR}/utils.o
OCLIENTE = ${ODIR}/main_cliente.o ${ODIR}/cliente.o ${ODIR}/utils.o
LDFLAGS = -lpthread -lrt
TESTFLAGS = `pkg-config --cflags --libs check`

all: cliente.app servidor.app

servidor.app: $(OSERVIDOR)
	$(CC) $(CFLAGS) -o servidor.app $(OSERVIDOR) $(LDFLAGS)

$(ODIR)/main_servidor.o: $(SDIR)/main_servidor.c
	mkdir -p ${ODIR}
	$(CC) $(CFLAGS) -c $(SDIR)/main_servidor.c -o $(ODIR)/main_servidor.o

$(ODIR)/servidor.o: $(SDIR)/servidor.c
	mkdir -p ${ODIR}
	$(CC) $(CFLAGS) -c $(SDIR)/servidor.c -o $(ODIR)/servidor.o

cliente.app: $(OCLIENTE)
	$(CC) $(CFLAGS) -o cliente.app $(OCLIENTE) $(LDFLAGS)

$(ODIR)/main_cliente.o: $(SDIR)/main_cliente.c
	mkdir -p ${ODIR}
	$(CC) $(CFLAGS) -c $(SDIR)/main_cliente.c -o $(ODIR)/main_cliente.o

$(ODIR)/cliente.o: $(SDIR)/cliente.c
	mkdir -p ${ODIR}
	$(CC) $(CFLAGS) -c $(SDIR)/cliente.c -o $(ODIR)/cliente.o

$(ODIR)/utils.o: $(SDIR)/utils.c
	mkdir -p ${ODIR}
	$(CC) $(CFLAGS) -c $(SDIR)/utils.c -o $(ODIR)/utils.o

testcliente: $(ODIR)/utils.o
	$(CC) $(CFLAGS) -o cliente_check.exe $(TESTDIR)/cliente_check.c $(ODIR)/cliente.o $(ODIR)/utils.o $(TESTFLAGS)
	./cliente_check.exe

testservidor:
	$(CC) $(CFLAGS) -o servidor_check.exe $(TESTDIR)/servidor_check.c $(ODIR)/servidor.o $(ODIR)/utils.o $(TESTFLAGS)
	./servidor_check.exe

test: testcliente testservidor

clean:
	rm -f $(ODIR)/*.o *.app *.exe *.xml *.log core

.PHONY: all test testcliente testservidor clean
